# Example Branching And GIT


Branching is the branching of the main timeline `master`.

### Mains commands:

```bash
# create a new branch from master 
# 0) you need to be in master and should be updated
$ git pull origin master

## Create you new branch 
### Use good nameing convention 
$ git checkout -b <branch-name>

### Name convention difer in teams
## one good thing is start with dev-feature

$ git checkout -b dev-front-page

# Make changes, git add and git commit.
# Once ready, push your branch 

$ git push origin dev-front-page



```


## New rules Not to get fired :D

Don't push to master. :)

If you are the devops engineer - DIFINATLY don't push to master.

Find out how to protect it.

And have jenkins be the only allow to edit master + senior engineer.


#### Extra notes:

Having naming convention can help you build actions on Jenkins
target anything saying dev-* to trigger CI
make another job to list to deploy-*